/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * Gonzalo Trossero - gonzalotrossero@gmail.com
 *
 *
 *
 * Revisión:
 * 28-05-20: Versión inicial
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
//#include "../../4_blinking_switch_timer/inc/blinking_switch_timer.h"       /* <= own header */
#include "../inc/analog_io_uart.h"

#include "systemclock.h"
#include "timer.h"
#include "led.h"
#include "switch.h"
#include "delay.h"
#include "uart.h"
#include "analog_io.h"

/*==================[macros and definitions]=================================*/
#define ON 1
#define OFF 0
#define BUFFER_SIZE 231
#define T_DAC 4
#define T_ADC 2
#define BAUDIOS 115200
#define TS 0.002
/*==================[internal data definition]===============================*/
uint8_t DAC_end = FALSE;
uint8_t ADC_end = FALSE;
uint8_t ConvAD_End = FALSE;
uint16_t dato = 0;
float dato_filtrado = 0;
float filt_anterior = 0;
float alfa;
float fc = 100;
float RC;
uint8_t filtrar = OFF;
const char ecg[BUFFER_SIZE]={
		76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
		99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
		94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
		88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
		83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
		79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
		80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
		86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
		180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
		89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
		94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
		103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
		100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
		99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
		74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
};
/*==================[internal functions declaration]=========================*/
void TimerDAC(void);
void TimerADC(void);
void doADC(void);
uint16_t LowPass(uint16_t filtrado_anterior,uint16_t dato_sin_filtrar);
void do_Tec1(void);
void do_Tec2(void);
void do_Tec3(void);
void do_Tec4(void);
void SysInit(void);
/*==================[external data definition]===============================*/
timer_config my_timer_da = {TIMER_B,T_DAC,&TimerDAC}; /* fs = 250 Hz */
timer_config my_timer_ad = {TIMER_A,T_ADC,&TimerADC}; /* fs = 500 Hz */
serial_config my_uart_pc = {SERIAL_PORT_PC, BAUDIOS, NULL}; /* Estructura para datos serie*/
analog_input_config my_ADC = {CH1 , AINPUTS_SINGLE_READ , &doADC}; /* Estructura para ADC */
/*==================[external functions definition]==========================*/
/* Gestiona interrupción para DAC */
void TimerDAC(void)
{
	DAC_end = TRUE;
}

/* Gestiona interrupción para ADC */
void TimerADC(void)
{
	ADC_end = TRUE;
}

/* Gestiona conversión AD */
void doADC(void)
{
	AnalogInputRead(my_ADC.input,&dato);
	ConvAD_End = TRUE;
}

/* Permite activar el uso del filtro */
void do_Tec1(void)
{
	filtrar = ON;
}

/* Permite desactivar el uso del filtro */
void do_Tec2(void)
{
	filtrar = OFF;
}

/* Permite desminuir la frecuencia de corte del filtro */
void do_Tec3(void)
{
	if(fc > 10)
	{
		fc -= 10;
	}
}

/* Permite incrementar la frecuencia de corte del filtro */
void do_Tec4(void)
{
	fc += 10;
}

/* Filtro Pasa-Bajo */
uint16_t LowPass(uint16_t filtrado_anterior,uint16_t dato_sin_filtrar)
{
	RC = 1/(2*(3.14)*fc);
	uint16_t filtrado_actual = 0;
	alfa = TS/(RC + TS);
	filtrado_actual = filtrado_anterior + alfa*(dato_sin_filtrar - filtrado_anterior);
	return filtrado_actual;
}

/*Inicializaciones*/
void SysInit(void)
{
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	SwitchActivInt(SWITCH_1, do_Tec1);
	SwitchActivInt(SWITCH_2, do_Tec2);
	SwitchActivInt(SWITCH_3, do_Tec3);
	SwitchActivInt(SWITCH_4, do_Tec4);
	TimerInit(&my_timer_da);
	TimerInit(&my_timer_ad);
	TimerStart(TIMER_A);
	TimerStart(TIMER_B);
	UartInit(&my_uart_pc);
	AnalogInputInit(&my_ADC);
	AnalogOutputInit();
}

int main(void)
{
	SysInit();
	uint16_t pos = 0;

	while(1)
	{
		/* Conversión AD */
		if(ADC_end)
		{
			AnalogStartConvertion();
			ADC_end = FALSE;
		}
		/* Procesamiento y envio de datos */
		if(ConvAD_End)
		{
			if(!filtrar)
			{
				UartSendString(my_uart_pc.port,UartItoa(dato,10));
				UartSendString(my_uart_pc.port,"\r");
			}
			if(filtrar)
			{
			   dato_filtrado = LowPass(filt_anterior,dato);

			   UartSendString(my_uart_pc.port,UartItoa(dato,10));
			   UartSendString(my_uart_pc.port," , ");
			   UartSendString(my_uart_pc.port,UartItoa(dato_filtrado,10));
			   UartSendString(my_uart_pc.port,"\r");
			   filt_anterior = dato_filtrado;
			}

			ConvAD_End = FALSE;
		}
		/* Conversión DA */
		if(DAC_end)
		{
			AnalogOutputWrite(ecg[pos]);
			pos++;
			if(pos == BUFFER_SIZE)
			{
				pos = 0;
			}

			DAC_end = FALSE;
		}
	}
}

/*==================[end of file]============================================*/

