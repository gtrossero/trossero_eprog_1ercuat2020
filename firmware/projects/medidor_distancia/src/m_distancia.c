/* FIUNER - 2020
 * Autor:
 * Gonzalo Trossero - gonzalotrossero@gmail.com
 *
 *
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/m_distancia.h"       /* <= own header */
#include "systemclock.h"
#include "hc_sr4.h"
#include "led.h"
#include "timer.h"
#include "switch.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/
#define OFF 0
#define ON 1
uint8_t control = OFF;
uint8_t hold = OFF;
uint8_t timer_control = OFF;
/*==================[internal data definition]===============================*/
int32_t distance;
int8_t dato_recibido;

void FuncTec1()
{
	control =! control;
}

void FuncTec2()
{
	hold =! hold;
}
/*==================[internal functions declaration]=========================*/
void do_refresh(void);
void do_uart(void);
/*==================[external data definition]===============================*/
timer_config my_timer = {TIMER_A,1000,&do_refresh}; /* Estructura typdef */
serial_config my_uart_pc = {SERIAL_PORT_PC, 115200, do_uart}; /* Estructura para datos serie*/
/*==================[external functions definition]==========================*/

/* do_uart activa/detiene medición y la pone en modo hold
 * Se llama cada vez que entra un dato en estructura my_uart_pc*/
void do_uart(void)
{
	UartReadByte(SERIAL_PORT_PC, &dato_recibido);
	if(dato_recibido == 'O')
	{
		FuncTec1();
	}
	if(dato_recibido == 'H')
	{
		FuncTec2();
	}
}

/* do_refresh cambia estado de timer_control a ON
 * Se llama cada 1000 ms en my_timer gestionando muestreo de medición*/
void do_refresh(void)
{
	timer_control = ON;
}

int main(void)
{
	SystemClockInit();
	HcSr04Init(T_FIL2,T_FIL3);
	LedsInit();
	SwitchesInit();
	SwitchActivInt(SWITCH_1,FuncTec1);
	SwitchActivInt(SWITCH_2,FuncTec2);
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	UartInit(&my_uart_pc);

    while(1)
    {
     if(timer_control) /* timer_control cambia a ON cada 1s */
     {
      	distance = HcSr04ReadDistanceCentimeters();
       /* Medición y visualización por LEDs */
    	if(control)
    	{
       		if(distance<=10 && !hold) /* Rango 1 */
    		{
    			LedOff(LED_1);
    			LedOff(LED_2);
    			LedOff(LED_3);
    			LedOn(LED_RGB_B);
    		}
    		if(distance>10 && distance<=20 && !hold) /* Rango 2 */
    		{
    			LedOff(LED_2);
    			LedOff(LED_3);
    			LedOn(LED_RGB_B);
    			LedOn(LED_1);
    		}
    		if(distance>20 && distance<=30 && !hold) /* Rango 3 */
    		{
    			LedOff(LED_3);
    			LedOn(LED_RGB_B);
    			LedOn(LED_1);
    			LedOn(LED_2);
    		}
    		if(distance>30 && !hold) /* Rango 4 */
    		{
    			LedOn(LED_RGB_B);
    			LedOn(LED_1);
    			LedOn(LED_2);
    			LedOn(LED_3);
    		}
        	/* Envio de datos */
            if(!hold)
            {
        	UartSendString(my_uart_pc.port,UartItoa(distance, 10));
        	UartSendString(my_uart_pc.port, " cm\r\n");
            }
        } /* Fin if(control) */
    	if(control == OFF)
    	{
    		LedOff(LED_RGB_B);
    		LedOff(LED_1);
    		LedOff(LED_2);
    		LedOff(LED_3);
       	} /* Fin if(control == OFF) */

    	timer_control = OFF;
     } /* Fin if(timer_control) */
    }

    HcSr04Deinit(T_FIL2,T_FIL3);
	return 0;
}

/*==================[end of file]============================================*/


