medidor_distancia
Mide distancia mediante un sensor de ultrasonido

El funcionamiento puede verse en el siguiente video de YouTube:
https://www.youtube.com/watch?v=wYHbugmI3zs

El funcionamiento integrando la comunicación puerto serie puede verse en el siguiente video de YouTube:
https://www.youtube.com/watch?v=80U3OQyi7oQ