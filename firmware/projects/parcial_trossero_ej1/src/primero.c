/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2020
 * Autor:
 * Gonzalo Trossero - gonzalotrossero@gmail.com
 *
 *
 *
 * Revisión:
 * 09-06-20: Versión inicial
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../parcial_trossero_ej1/inc/primero.h"       /* <= own header */
#include "systemclock.h"
#include "hc_sr4.h"
#include "led.h"
#include "timer.h"

/*==================[macros and definitions]=================================*/
#define ON 1
#define OFF 0
#define T_MIN 200
/*==================[internal data definition]===============================*/
uint8_t control = OFF;
uint8_t hold = OFF;
uint8_t timer_control = OFF;
int32_t distance;
int8_t dato_recibido;
uint32_t cont = 0;
uint32_t j=0;
/*==================[internal functions declaration]=========================*/
void do_blink(void);
/*==================[external data definition]===============================*/
timer_config my_timer = {TIMER_A,T_MIN,&do_blink}; /* Estructura typdef */
/*==================[external functions definition]==========================*/


void do_blink(void)
{
	cont ++;
}

void periodo(void)
{

}

int main(void)
{
	SystemClockInit();
	HcSr04Init(T_FIL2,T_FIL3);
	LedsInit();
	TimerInit(&my_timer);
	TimerStart(TIMER_A);

    while(1)
    {
      	distance = HcSr04ReadDistanceCentimeters();
      	if(distance <= 4)
      	{cont = 1;}
      	if(distance > 4 && distance <= 8)
      	{cont = 2;}
      	if(distance > 8 && distance <= 12)
      	{cont = 3;}
      	if(distance > 12 && distance <= 16)
      	{cont = 4;}
      	if(distance > 16 && distance <= 20)
      	{cont = 5;}


      	for(cont; cont>0; cont-- )
      	{
      		LedToggle(LED_3);
      	}

    }

    HcSr04Deinit(T_FIL2,T_FIL3);
	return 0;
}
/*==================[end of file]============================================*/

