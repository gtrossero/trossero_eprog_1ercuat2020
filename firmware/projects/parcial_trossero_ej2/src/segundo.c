/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2020
 * Autor:
 * Gonzalo Trossero - gonzalotrossero@gmail.com
 *
 *
 *
 * Revisión:
 * 09-06-20: Versión inicial
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../parcial_trossero_ej2/inc/segundo.h"       /* <= own header */
#include "systemclock.h"
#include "timer.h"
#include "analog_io.h"
#include "uart.h"


/*==================[macros and definitions]=================================*/
#define T_ADC 50
#define T_PROM 1000
#define BAUDIOS 115200
/*==================[internal data definition]===============================*/
uint16_t prom = 0;
uint16_t acum = 0;
uint16_t dato = 0;
uint8_t ADC_End = 0;
uint8_t ConvAD_End = 0;
/*==================[internal functions declaration]=========================*/
void TimerADC(void);
void doADC(void);
void Promedio(void);
/*==================[external data definition]===============================*/
timer_config my_timer_prom = {TIMER_A,T_PROM,&Promedio}; /* f = 1 Hz  */
timer_config my_timer_ad = {TIMER_B,T_ADC,&TimerADC}; /* f = 20 Hz  */
serial_config my_uart_pc = {SERIAL_PORT_PC, BAUDIOS, NULL}; /* Estructura para datos serie*/
analog_input_config my_ADC = {CH1 , AINPUTS_SINGLE_READ , &doADC}; /* Estructura para ADC */
/*==================[external functions definition]==========================*/

/* Gestiona interrupción para ADC */
void TimerADC(void)
{
	ADC_End = TRUE;
}

/* Gestiona conversión AD */
void doADC(void)
{
	AnalogInputRead(my_ADC.input,&dato);
	ConvAD_End = TRUE;
}

/* Calcula el promedio del valor digital acumulado en un segundo (20 muestras)*/
void Promedio (void)
{
	prom = acum/20;
	acum = 0;
}


int main(void)
{
	SystemClockInit();
	TimerInit(&my_timer_prom);
	TimerStart(TIMER_A);
	TimerInit(&my_timer_ad);
    TimerStart(TIMER_B);
	UartInit(&my_uart_pc);
	AnalogInputInit(&my_ADC);
	AnalogOutputInit();

	while(1)
	{
		/* Conversión AD */
		if(ADC_End)
		{
			AnalogStartConvertion();
			ADC_End = FALSE;
		}
		/* Al final la Conversion AD se envia el promedio calculado cada 1 s */
		if(ConvAD_End)
		{
			acum = acum + dato;

			UartSendString(my_uart_pc.port,UartItoa(prom,10));
			UartSendString(my_uart_pc.port,"\r");
		}
	}


}
/*==================[end of file]============================================*/

