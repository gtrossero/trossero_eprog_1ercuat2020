/* Copyright 2020,
 * Gonzalo Trossero
 * gonzalotrossero@gmail.com
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @brief Bare Metal driver for switchs in the EDU-CIAA board.
 **
 **/


/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20200430 v0.1 initials initial version
 */

/*==================[inclusions]=============================================*/
#include "hc_sr4.h"
#include "gpio.h"
#include "delay.h"
#include "led.h"
/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/
gpio_t Echo; gpio_t Trigger;
/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/
bool HcSr04Init(gpio_t echo, gpio_t trigger)
{
	Echo=echo;
	Trigger=trigger;
	/* Configuration of the GPIO */
	GPIOInit(echo, GPIO_INPUT);
	GPIOInit(trigger, GPIO_OUTPUT);
	/* Led indicator */
	LedsInit();
	LedOn(LED_1);
	DelayMs(250);
	LedOff(LED_1);
	DelayMs(250);
	LedOn(LED_2);
	DelayMs(250);
	LedOff(LED_2);
	DelayMs(250);
	LedOn(LED_3);
	DelayMs(250);
	LedOff(LED_3);
	DelayMs(250);

	return true;
}

int16_t HcSr04ReadDistanceCentimeters(void)
{
	int16_t Distance_Cm=0, us=0, control=1;
	GPIOOn(Trigger);
	DelayUs(10);
	GPIOOff(Trigger);
	/*Wait echo pulse*/
	while(!GPIORead(Echo))
	{}
	/*Count us T_FIL2 is high*/
	while(control)
	{
		while(GPIORead(Echo))
		{
			DelayUs(1);
			us++;
		}
	control=0;
	}
	Distance_Cm=us/27;
	return Distance_Cm;
}

int16_t HcSr04ReadDistanceInches(void)
{
	int16_t Distance_In=0, us=0, control=1;
		GPIOOn(Trigger);
		DelayUs(10);
		GPIOOff(Trigger);
		/*Wait echo pulse*/
		while(!GPIORead(Echo))
		{}
		/*Count us T_FIL2 is high*/
		while(control)
		{
			while(GPIORead(Echo))
			{
				DelayUs(1);
				us++;
			}
		control=0;
		}
		Distance_In=us/74;
		return Distance_In;
}

bool HcSr04Deinit(gpio_t echo, gpio_t trigger)
{
	return true;
}

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
