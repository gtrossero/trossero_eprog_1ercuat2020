/* Copyright 2020,
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */



/** @brief Distance Meter
 **
 ** This is a driver for a ultrasound sensor configured to measure distance
 **
 **/

/** @addtogroup CIAA_Firmware CIAA Firmware
 ** @{ */

/** @addtogroup Sources_LDM Leandro D. Medus Sources
 ** @{ */
/** @addtogroup Baremetal Bare Metal source file
 ** @{ */


/*==================[inclusions]=============================================*/
#include <stdint.h>
#include "gpio.h"
/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/


/*==================[external functions declaration]=========================*/
/** @brief Initialization function of pins
 * This function initialize the pins T_FIL2 and T_FIL3 present in the EDU-CIAA board
 * @return true if no error
 */
bool HcSr04Init(gpio_t echo, gpio_t trigger);

/** @brief Function to read distance in centimeters
 * The function returns a 16 bits word that represents the distance in centimeters measured
 * @return 0 if no key pressed, SWITCH_1 SWITCH_2 SWITCH_3 SWITCH_4 in other case.
 */
int16_t HcSr04ReadDistanceCentimeters(void);

/** @brief Function to read distance in centimeters
 * The function returns a 16 bits word that represents the distance in inches measured
 * @return 0 if no key pressed, SWITCH_1 SWITCH_2 SWITCH_3 SWITCH_4 in other case.
 */
int16_t HcSr04ReadDistanceInches(void);

/** @brief Deinitialization function of pins
 * This function deinitialize the pins T_FIL2 and T_FIL3 present in the EDU-CIAA board
 * @return true if no error
 */
bool HcSr04Deinit(gpio_t echo, gpio_t trigger);

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/


