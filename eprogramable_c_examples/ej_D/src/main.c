/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "stdint.h"
#include "main.h"

#include <stdio.h>
#include<math.h>
/*==================[macros and definitions]=================================*/

/*==================[internal functions declaration]=========================*/

typedef struct
{
	uint8_t port;
    uint8_t pin;
	uint8_t dir;
} gpioConf_t;

void  DecToBcd (uint32_t data, uint8_t digits, uint8_t *bcd_number){

	uint8_t i;
	uint32_t aux;
	for(i=0; i<digits; i++){
		aux=pow(10,i);
		bcd_number[i] = (data / aux) % 10;/*/1 primer digito, /10 segundo digito...*/
	}
}/*Fin funcion DecToBcd*/

void Port(uint8_t data,gpioConf_t *vector_struc){
	printf("Colocar puerto en %d \r\n",data);

					/* Puerto 1.4 */
	if(data & 1){
		printf("b0 ---> Puerto %d.%d en ALTO \r\n",vector_struc[0].port,vector_struc[0].pin);
	}
	else{
		printf("b0 ---> Puerto %d.%d en BAJO \r\n",vector_struc[0].port,vector_struc[0].pin);
	}

					/* Puerto 1.5 */
	if(data & 2){
		printf("b1 ---> Puerto %d.%d en ALTO \r\n",vector_struc[1].port,vector_struc[1].pin);
	}
	else{
		printf("b1 ---> Puerto %d.%d en BAJO \r\n",vector_struc[1].port,vector_struc[1].pin);
	}

					/* Puerto 1.6 */
	if(data & 4){
		printf("b2 ---> Puerto %d.%d en ALTO \r\n",vector_struc[2].port,vector_struc[2].pin);
	}
	else{
		printf("b2 ---> Puerto %d.%d en BAJO \r\n",vector_struc[2].port,vector_struc[2].pin);
	}

					/* Puerto 2.14 */
	if(data & 8){
		printf("b3 ---> Puerto %d.%d en ALTO \r\n",vector_struc[3].port,vector_struc[3].pin);
	}
	else{
		printf("b3 ---> Puerto %d.%d en BAJO \r\n",vector_struc[3].port,vector_struc[3].pin);
	}
	printf("\n");

}/*Fin funcion Port*/

int main(void)
{
	gpioConf_t vec_struc [] = {{1,4,1},{1,5,1},{1,6,1},{2,14,1}};
	uint32_t num;
	uint8_t dig,i;
	num=1234;
	dig=4;
	uint8_t bcd [dig];
	uint8_t *p=&bcd[0];

	DecToBcd(num,dig,p);

	for(i=0; i<dig; i++){
		Port(bcd[i],vec_struc);
	}



		return 0;
}

/*==================[end of file]============================================*/

