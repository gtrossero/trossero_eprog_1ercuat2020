########################################################################
#Cátedra: Electrónica Programable
#FIUNER
#2018
#Autor/es:
#JMreta - jmreta@ingenieria.uner.edu.ar
#
#Revisión:
########################################################################

# Ruta del Proyecto
####Ejemplo 1: Hola Mundo
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = 1_hola_mundo
#NOMBRE_EJECUTABLE = hola_mundo.exe

####Ejemplo 2: Control Mundo
#PROYECTO_ACTIVO = 2_control_mundo
#NOMBRE_EJECUTABLE = control_mundo.exe

####Ejemplo 3: Promedio
#PROYECTO_ACTIVO = 3_promedio_1
#NOMBRE_EJECUTABLE = promedio.exe

####x_proyecto_vacio
#PROYECTO_ACTIVO = x_proyecto_vacio
#NOMBRE_EJECUTABLE = vacio.exe

####proyecto_:Ej no obligatorios
#PROYECTO_ACTIVO = ej_no_oblig
#NOMBRE_EJECUTABLE = ejs.exe

####Ejercico 7
#PROYECTO_ACTIVO = ej_7
#NOMBRE_EJECUTABLE = ej_7.exe

####Ejercico 9
#PROYECTO_ACTIVO = ej_9
#NOMBRE_EJECUTABLE = ej_9.exe

####Ejercico 12
#PROYECTO_ACTIVO = ej_12
#NOMBRE_EJECUTABLE = ej_12.exe

####Ejercico 14
#PROYECTO_ACTIVO = ej_14
#NOMBRE_EJECUTABLE = ej_14.exe

####Ejercico 16
PROYECTO_ACTIVO = ej_16
NOMBRE_EJECUTABLE = ej_16.exe

#PROYECTO_ACTIVO = 5_menu
#NOMBRE_EJECUTABLE = menu.exe


#PROYECTO_ACTIVO = Ej_c_d_e
#NOMBRE_EJECUTABLE = c_d_e.exe