/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../ej_12/inc/main.h"

#include "stdint.h"
#include <stdio.h>
#include <string.h>
/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/
typedef struct {
	char nombre[12];
	char apellido[20];
	uint8_t edad;
   }Alumno_t;

int main(void)
{
//------------Con Puntero-----------//

	   Alumno_t *german;

	   strcpy(german->nombre,"German");
	   strcpy(german->apellido,"Perez");
	   german->edad=22;

	   printf("%s\n",german->nombre);
	   printf("%s\n",german->apellido);
	   printf("%d\n",german->edad);

	   printf("%s\n","");

//------------Sin Puntero-----------//

	   Alumno_t juan;

	   strcpy(juan.nombre,"Juan");
	   strcpy(juan.apellido,"Gonzalez");
	   juan.edad=18;

	   printf("%s\n",juan.nombre);
	   printf("%s\n",juan.apellido);
	   printf("%d\n",juan.edad);

		return 0;
}

/*==================[end of file]============================================*/

