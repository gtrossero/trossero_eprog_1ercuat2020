/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../ej_12/inc/main.h"

#include "stdint.h"
#include <stdio.h>
/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/
uint32_t var=0x01020304;
uint8_t a_1,a_2,a_3,a_4;
uint32_t masc1=(1<<2);

typedef union{
				  struct{
					uint8_t byte1;
					uint8_t byte2;
					uint8_t byte3;
					uint8_t byte4;
				}todos_los_bytes;

				uint32_t num;
			}completo;

int main(void){

	//---------a----------//

		a_1=var &  masc1;//00000100
		a_2=var>>8;//00000011
		a_3=var>>16;//00000010
		a_4=var>>24;//00000001

		printf("%d\n",a_1);
		printf("%d\n",a_2);
		printf("%d\n",a_3);
		printf("%d\n\n",a_4);

		//---------b----------//

		completo x;
		x.num=var;

		printf("%d\n",x.todos_los_bytes.byte1);
		printf("%d\n",x.todos_los_bytes.byte2);
		printf("%d\n",x.todos_los_bytes.byte3);
		printf("%d\n",x.todos_los_bytes.byte4);

		return 0;
}

/*==================[end of file]============================================*/

