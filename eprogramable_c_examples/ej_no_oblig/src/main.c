/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../ej_no_oblig/inc/main.h"

#include "stdint.h"
#include <stdio.h>
/*==================[macros and definitions]=================================*/
#define BIT6 6

/*==================[internal functions declaration]=========================*/
const uint32_t var1= 1<<BIT6;

//7
uint32_t var7;
const uint32_t masc7_1= ~((1<<13)|(1<<14));
const uint32_t masc7_2=(1<<3);
/*-----------------Ejercicios de repaso del proyecto 1--------------*/
int main(void)
{
	//---------1------------

  /*  const int32_t var1= 1<<6;//el bit menos sig. es el nro 0

     printf("%d\n",var1);

     //---------2------------

     const int16_t var2=3<<3;

     printf("%d\n",var2);

	//---------3------------

	uint16_t var3=0xFFFF;
    uint16_t masc3=0xBFFF;

    var3=var3 & masc3;

	printf("%d\n",var3);

	//---------4------------

	uint32_t var4=0x0000;
	uint32_t masc4=1<<2;

	var4=var4 | masc4;

	printf("%d\n",var4);

	//---------5------------

	uint32_t var5=0x00001234;
	uint32_t masc5=1;

	var5=var5 | masc5;

	printf("%d\n",var5);

	//---------6-----------

	uint32_t var6;
	uint32_t masc6=1<<4;
	masc6=~masc6;

	var6=var6 & masc6;

	printf("%d\n",var6);	*/


	//---------1------------

       //printf("%d\n",var1);

   //---------7------------
	var7=var7 & masc7_1;
	var7=var7 | masc7_2;

	printf("%d\n",var7);

		return 0;
}

/*==================[end of file]============================================*/

